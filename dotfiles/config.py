# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from libqtile.config import Key, Screen, Group, Drag, Click
from libqtile.lazy import lazy
from libqtile import layout, bar, widget

from typing import List  # noqa: F401

mod = "mod4"

keys = [
    # Switch between windows in current stack pane
    Key([mod], "k", lazy.layout.down()),
    Key([mod], "j", lazy.layout.up()),

    # Move windows up or down in current stack
    Key([mod, "control"], "k", lazy.layout.shuffle_down()),
    Key([mod, "control"], "j", lazy.layout.shuffle_up()),

    # Switch window focus to other pane(s) of stack
    Key([mod], "space", lazy.layout.next()),

    # Swap panes of split stack
    Key([mod, "shift"], "space", lazy.layout.rotate()),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"], "Return", lazy.layout.toggle_split()),
    Key([mod], "Return", lazy.spawn("xterm")),

    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout()),
    Key([mod], "w", lazy.window.kill()),

    Key([mod, "control"], "r", lazy.restart()),
    Key([mod, "control"], "q", lazy.shutdown()),
    Key([mod], "r", lazy.spawncmd()),

	#Custom application spawns
	Key([mod], 'd', lazy.spawn('discord')),
	Key([mod], 'e', lazy.spawn('thunderbird')),
	Key([mod], 'f', lazy.spawn('firefox')),
	#Key([mod], 'f', 'p', lazy.spawn('firefox'))
]


#Group Sorting

group_names = [
	("Email"),
	("Config"),
	("Web")
]

#Group('Email', layout='monadtall')
#Group('Config', layout='monadtall')
#Group('Web', layout='monadtall')

#groups = [Group(i) for i in "uiop"]

groups  = [
	Group('Email', layout='max'),
	Group('Config', layout='monadtall'),
	Group('Web', layout='monadtall'),
	Group('Work', layout='monadtall'),
	Group('Social', layout='max'),
]


#for i in groups:
#    keys.extend([
		
        # mod1 + letter of group = switch to group
#        Key([mod], i.name, lazy.group[i.name].toscreen()),

        # mod1 + shift + letter of group = switch to & move focused window to group
#        Key([mod, "shift"], i.name, lazy.window.togroup(i.name, switch_group=True)),
        # Or, use below if you prefer not to switch to that group.
        # # mod1 + shift + letter of group = move focused window to group
        # Key([mod, "shift"], i.name, lazy.window.togroup(i.name)),
#    ])

keys.extend([
	Key([mod], 'y', lazy.group['Email'].toscreen()),
	Key([mod], 'u', lazy.group['Config'].toscreen()),
	Key([mod], 'i', lazy.group['Web'].toscreen()),
	Key([mod], 'o', lazy.group['Work'].toscreen()),
	Key([mod], 'p', lazy.group['Social'].toscreen()),

	Key([mod, 'shift'], 'y', lazy.window.togroup('Email')),
	Key([mod, 'shift'], 'u', lazy.window.togroup('Config')),
	Key([mod, 'shift'], 'i', lazy.window.togroup('Web')),
	Key([mod, 'shift'], 'o', lazy.window.togroup('Work')),
	Key([mod, 'shift'], 'p', lazy.window.togroup('Social'))
])

layouts = [
    layout.Max(),
    #layout.Stack(num_stacks=2),
    # Try more layouts by unleashing below layouts.
    # layout.Bsp(),
    # layout.Columns(),
    # layout.Matrix(),
     layout.MonadTall(border_width=1,margin=4),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

widget_defaults = dict(
    font='sans',
    fontsize=13,
    padding=20,
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
		wallpaper='~/images/Arch-Linux-Full-HD-Wallpaper.jpg',
		wallpaper_mode='fill',
        top=bar.Bar(
			[
				widget.WindowName(),
				widget.Prompt(),
				widget.Clock(center_aligned=True, format='%m/%d/%y %a %I:%m %p'),
			],
			34,

		),

		bottom=bar.Bar(
            [
                widget.CurrentLayout(padding=5),
                widget.GroupBox(padding=5),
				widget.Battery(),
                widget.Volume(padding=10, volume_down_command='amixer set Master 2dB-', volume_up_command='amixer set Master 2dB+'),
				widget.QuickExit(),
            ],
           34,
		   background=["#212F3C",'#000000']
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button2", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button3", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    {'wmclass': 'confirm'},
    {'wmclass': 'dialog'},
    {'wmclass': 'download'},
    {'wmclass': 'error'},
    {'wmclass': 'file_progress'},
    {'wmclass': 'notification'},
    {'wmclass': 'splash'},
    {'wmclass': 'toolbar'},
    {'wmclass': 'confirmreset'},  # gitk
    {'wmclass': 'makebranch'},  # gitk
    {'wmclass': 'maketag'},  # gitk
    {'wname': 'branchdialog'},  # gitk
    {'wname': 'pinentry'},  # GPG key password entry
    {'wmclass': 'ssh-askpass'},  # ssh-askpass
])
auto_fullscreen = True
focus_on_window_activation = "smart"

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
